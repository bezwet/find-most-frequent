package test;

import main.MostFrequent;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MostFrequentTests {

    @ParameterizedTest(name = "Array consists of specified numbers {0}. Number {1} is the most frequent.")
    @MethodSource("findMostFrequentArguments")
    public void findMostFrequent_test(int[] arr, Integer expectedResult) {
        MostFrequent mostFrequent = new MostFrequent();

        Integer mostFrequentNumber = mostFrequent.findMostFrequent(arr);
        assertEquals(expectedResult, mostFrequentNumber);
    }

    public static Stream<Arguments> findMostFrequentArguments() {
        return Stream.of(
                Arguments.of(new int[]{10, 5, 2, 2}, 2),
                Arguments.of(new int[]{100, 100}, 100),
                Arguments.of(null, -1),
                Arguments.of(new int[]{}, -1));
    }
}
