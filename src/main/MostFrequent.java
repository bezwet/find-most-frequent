package main;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MostFrequent {

    public Integer findMostFrequent(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            return -1;
        }
        Map<Integer, Long> mapOfNumbersOccurrence = Arrays.stream(numbers)
                .boxed()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return Collections.max(mapOfNumbersOccurrence.entrySet(), Map.Entry.comparingByValue()).getKey();
    }
}
