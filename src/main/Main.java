package main;

public class Main {
    public static void main(String[] args) {
        MostFrequent mostFrequent = new MostFrequent();
        Integer mostFrequentNumber = mostFrequent.findMostFrequent(new int[]{1, 3, 5, 5, 7, 5, 6, 10});

        System.out.println(mostFrequentNumber);
    }
}